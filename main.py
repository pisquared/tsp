from math import factorial
from time import time

import numpy as np

DEBUG = False


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_distance(self, other):
        return np.sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2)


def generate_perms(a):
    if len(a) == 2:
        return [
            [a[0], a[1]],
            [a[1], a[0]],
        ]
    rv = []
    for i in a:
        rest = list(set(a) - set([i]))
        for j in generate_perms(rest):
            rv.append([i] + j)
    return rv


def measure_distance(P):
    d = []
    for a_i, p1 in enumerate(P):
        d.append([])
        for p2 in P[a_i + 1:]:
            a = Point(p1[0], p1[1])
            b = Point(p2[0], p2[1])
            d[a_i].append(a.get_distance(b))

    return d


def get_distance(d, start, end):
    if start > end:
        start, end = end, start
    # off by 1 because of indexing
    # and off by start because of incomplete matrix
    return d[start][end - start - 1]


def main(start_point):
    distances_points = measure_distance(P)
    if DEBUG:
        print(distances_points)
    all_vertices = list(range(len(P)))
    rest_vertices = set(all_vertices) - set([start_point])
    all_perms = generate_perms(rest_vertices)
    all_paths = [[start_point] + perm + [start_point] for perm in all_perms]
    min_path_length = float('inf')
    min_path = []
    for path in all_paths:
        path_length = 0
        for vertex in range(len(path) - 1):
            edge = get_distance(distances_points, path[vertex], path[vertex + 1])
            path_length += edge
        if DEBUG:
            print("Path is: {} with distance: {}".format(path, path_length))
        if path_length < min_path_length:
            min_path_length = path_length
            min_path = path
    print("Min path is: {} with distance: {}".format(min_path, min_path_length))


if __name__ == "__main__":
    P = [
        (1, 1),
        (7, 2),
        (1, 3),
        (7, 4),
        (1, 5),
        (7, 5),
        (1, 4),
        (7, 3),
        (1, 2),
        (7, 1),
    ]
    print("Points: N={}, N!={} ".format(len(P), factorial(len(P))))
    start = time()
    main(0)
    end = time() - start
    print("Took {} s".format(end))


# 10 точки отнемат около 2 секунди, 11 отнемат 11*2 ~= 29 секунди...